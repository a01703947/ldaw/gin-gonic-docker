# Docker-ized Gin Gonic
Gin Gonic is a lightweight HTTP/HTTPS web server framework for Python. This is just an example of a `Dockerfile` to build and run an instance.

This was done on macOS 10.15.3 (Catalina) so your mileage may vary depending on your host OS. 
__DISCLAIMER:__ This is purely didactic and is not intended for production.

## Building
```shell
docker build -t gin-gonic .
```

## Running
```shell
docker run -Pd --rm --name gin-gonic gin-gonic
```
### Retrieve ports
```shell
docker port gin-gonic
```

## Testing in browser
You have to retrieve on what IP is your docker machine running. Usually you can finding by issuing:
```shell
echo $DOCKER_HOST | grep -oe '\([0-9]\{1,3\}\.\)\{3\}[0-9]\{1,3\}'
```
Using the ip and the port from the port before you can preview what's running on the image.
