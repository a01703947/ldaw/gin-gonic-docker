FROM ubuntu:19.04

RUN  apt-get update
RUN  apt-get install git wget -y

RUN cd /tmp
RUN wget https://dl.google.com/go/go1.11.linux-amd64.tar.gz

RUN  tar -xvf go1.11.linux-amd64.tar.gz
RUN mv go /usr/local

ENV GOROOT /usr/local/go
ENV GOPATH $HOME/go
ENV PATH $GOPATH/bin:$GOROOT/bin:$PATH

EXPOSE 8080

RUN go get -u github.com/gin-gonic/gin
COPY . .

CMD go run hello_world.go
